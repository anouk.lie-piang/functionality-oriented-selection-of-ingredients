# Functionality-oriented selection of ingredients

This git project contains all scripts that were used to generate data for the paper named: Functionality-oriented selection of sustainable ingredients using data-driven modelling (2021).